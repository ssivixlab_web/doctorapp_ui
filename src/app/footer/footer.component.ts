import { Component, OnInit, Input } from '@angular/core';
import { AppPublishSubscribeService } from '../common/pub-sub/publish-subscribe.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Input() hideLinks;

  clinicServices: string[] = [
    'Malnutrition',
    'Opthalmologist',
    'Nephrologists',
    'Ear-nose-throat specialist',
    'Endocrinologists',
    'Cardiologist',
    'Endocrinologists',
    'Psyciatrist',
    'Neurologists',
    'General Physician'
  ];

  locationList: string[] = [
    'Singapore',
    'Bengaluru',
    'Delhi',
    'Chennai'
  ];

  constructor(private pubs: AppPublishSubscribeService,
              private router: Router) { }

  ngOnInit() {
  }

  getbyType(item) {
    const payload = {
      currentLocation: '',
      searchText: item,
      gender: '',
      sortFees: 'desc'
    };
    this.updateResult(payload);
  }

  getbyLocation(item) {
    const payload = {
      currentLocation: item,
      searchText: '',
      gender: '',
      sortFees: 'desc'
    };
    this.updateResult(payload);
  }

  updateResult(payload) {
    this.router.navigateByUrl('search');
    this.pubs.publishEvData('CATEGORY_CLICK', payload);

  }

}
