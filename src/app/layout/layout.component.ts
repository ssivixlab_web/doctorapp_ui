import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
   hideLinks: boolean = false;
  constructor(private router: Router ) {
    console.log(this.router.url);

   }

  ngOnInit() {
    this.hideLinks = this.router.url === '/search' ? false : true;
  }

}
