import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppProviderService } from '../dataService/data.service';

@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.scss']
})
export class EnquiryComponent implements OnInit {
  @Input() public selectedDoctor;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  enquiryForm: FormGroup;

  constructor(public activeModal: NgbActiveModal,
              private fb: FormBuilder,
              private appProviderService: AppProviderService) {
    this.enquiryForm = this.fb.group({
      name: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      message: new FormControl('', Validators.required),
      doctorId: new FormControl('', Validators.required)
    });
   }

  ngOnInit() {
    console.log(this.selectedDoctor);
    this.enquiryForm.get('doctorId').setValue(this.selectedDoctor._id);

  }

  passBack() {
    this.passEntry.emit(this.selectedDoctor);
  }

  submit(payload) {
    this.appProviderService.sendEnquiry(payload).subscribe({
      next: (res) => {
        if (res.status) {
          this.activeModal.close(res.success);
        }
      },
      error: (err: any) =>  {

      },
      complete: () => {

      }
    });
  }

}
