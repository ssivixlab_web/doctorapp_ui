import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { AppProviderService } from '../dataService/data.service';
import { AppPublishSubscribeService } from '../common/pub-sub/publish-subscribe.service';
import { MatRadioChange, MatAutocompleteSelectedEvent } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DownloadComponent } from '../download/download.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-nav',
  templateUrl: './search-nav.component.html',
  styleUrls: ['./search-nav.component.scss']
})
export class SearchNavComponent implements OnInit {
  defaultSGCountryBanner = true;
  countryCode = 'SG';
  searchform: FormGroup;
  isCollapsed = true;
  clinicServices: string[] = [
    'Malnutrition',
    'Opthalmologist',
    'Nephrologists',
    'Ear-nose-throat specialist',
    'Endocrinologists',
    'Cardiologist',
    'Endocrinologists',
    'Psyciatrist',
    'Neurologists',
    'General Physician'
  ];
  filteredOptions: Observable<string[]>;

  genderList = [
    {
      key: '',
      value: 'All'
    },
    {
      key: 'M',
      value: 'Male'
    },
    {
      key: 'F',
      value: 'Female'
    }
  ];

  priceList = [
    {
      key: 'asc',
      value: 'Low to High'
    },
    {
      key: 'desc',
      value: 'High to Low'
    }
  ];

  imageBanner = [
    {
      imgSrc: 'banner-1.jpg',
      countryCode: 'SG'
    },
    {
      imgSrc: 'banner-2.jpg',
      countryCode: 'SG'
    },
    {
      imgSrc: 'banner-3.jpg',
      countryCode: 'SG'
    },
    {
      imgSrc: 'banner-4.jpg',
      countryCode: 'IN'
    },
    {
      imgSrc: 'banner-5.jpg',
      countryCode: 'IN'
    },
    {
      imgSrc: 'banner-6.jpg',
      countryCode: 'IN'
    }
  ];

  @Output() dataEvent = new EventEmitter<any>();

  constructor(private appService: AppProviderService,
              private pubs: AppPublishSubscribeService,
              private modalService: NgbModal,
              private activatedRoute: ActivatedRoute) {
    this.appService.getGeoDetails();
  }

  ngOnInit() {
    this.searchform = new FormGroup({
      currentLocation: new FormControl(''),
      searchText: new FormControl(''),
      gender: new FormControl(''),
      sortFees: new FormControl('desc')
    });
    this.filteredOptions = this.searchform.get('searchText').valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    let defaultCall = true;

    this.pubs.subscribeEvData('CATEGORY_CLICK', (res) => {
      defaultCall = false;
      this.searchform.get('currentLocation').setValue(res.currentLocation);
      this.searchform.get('searchText').setValue(res.searchText);
      this.sendMessage();
    }, (err) => {}, () => {}, true);

    if (defaultCall) {
      this.setCurrentLocation();
    }

    this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.query !== '') {
        this.searchform.get('searchText').setValue(params.query);
      }
    });

  }

  sendMessage() {
    this.dataEvent.emit(this.searchform.value);
  }

  setCurrentLocation() {
    this.appService.currentLocation$.subscribe((res) => {
      if (res) {
        this.currentLocCb(res);
      }
    }, (err) => {
      this.currentLocCb(err);
    });
  }

  currentLocCb(res) {
    let city: string = 'Singapore';
    if (res && res.city && res.city !== '') {
      city = res.city;
      this.countryCode = res.country_code;
      if (res.country_code !== 'SG') {
        this.defaultSGCountryBanner = false;
      }
    }
    this.searchform.get('currentLocation').setValue(city);
    this.sendMessage();
  }

  buttonChangeEv(event: MatRadioChange) {
    setTimeout(() => {
      this.sendMessage();
    }, 100);
    this.isCollapsed = !this.isCollapsed;
  }

  optionSelEv(event: MatAutocompleteSelectedEvent) {
    this.sendMessage();
  }


  // async getCountryList() {
  //   return await create({
  //       baseURL: "https://restcountries-v1.p.rapidapi.com",
  //       timeout: 100000,
  //       headers: {
  //           "x-rapidapi-host": "restcountries-v1.p.rapidapi.com",
  //           "x-rapidapi-key": "acd742ccfcmshdb4bd0b759fc3b3p159d19jsn655755033519"
  //       }
  //   }).get("/all");
  // }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.clinicServices.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  openModal() {
    const modalRef = this.modalService.open(DownloadComponent);
   // modalRef.componentInstance.selectedDoctor = payload;
    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
    }, (err) => {
      console.log('err', err);
    });
  }

}
