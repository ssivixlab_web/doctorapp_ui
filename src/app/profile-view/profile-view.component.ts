import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppProviderService } from '../dataService/data.service';
import * as moment from 'moment';
import { Title, Meta } from '@angular/platform-browser';
import * as _ from 'lodash';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EnquiryComponent } from '../enquiry/enquiry.component';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit {
  doctorProfileData: any = {};
  @Input() data;

  constructor(private route: ActivatedRoute,
              private appService: AppProviderService,
              private titleService: Title,
              private meta: Meta,
              private modalService: NgbModal) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      if (params && params.id) {
        // get doctor data
        this.appService.getDoctorProfile(params.id).subscribe((res) => {
          this.doctorProfileData = res.data;
          this.makeSeo();
        });
      }
    });
  }

  makeSeo() {
    this.titleService.setTitle(`Doctor | ${this.doctorProfileData.name} | ${this.doctorProfileData.specialization}`);
    const keywords = `${this.doctorProfileData.specialization}, ${this.doctorProfileData.nameOfClinic}`;
    this.meta.addTag({name: 'description', content: `'${_.join(this.doctorProfileData.clinicServices, ', ')}'`});
    this.meta.addTag({name: 'keywords', content: keywords});
    this.meta.addTag({name: 'author', content: 'SSIVIXLAB'});
    this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

  getTime(res) {
    const startTime = moment(`${res.startHour}:${res.startMinute}`, 'HH:mm');
    const endTime = moment(`${res.endHour}:${res.endMinute}`, 'HH:mm');
    return {
      start: startTime,
      end: endTime
    };
  }

  contactClickCall(doc) {
    this.appService.updateClickContact(doc._id).subscribe((res) => {
      //
    });
  }


  openModal(payload) {
    const modalRef = this.modalService.open(EnquiryComponent);
    modalRef.componentInstance.selectedDoctor = payload;
    modalRef.result.then((result) => {
      if (result) {
        alert(result);
      }
    }, (err) => {
      console.log('err', err);
    });
  }
  isShown: boolean = false ;
  toggleShow() {
    this.isShown = ! this.isShown;
  }
}
