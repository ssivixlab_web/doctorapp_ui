import { Component, OnInit } from '@angular/core';
import { AppProviderService } from '../dataService/data.service';
import { Title, Meta } from '@angular/platform-browser';
import * as _ from 'lodash';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  doctorData: [];
  pageConfig = {
    itemsPerPage: 10,
    page: 1,
    totalItems: 0
  };
  filter = {
    currentLocation: '',
    searchText: '',
    sortFees: '',
    gender: '',
    pageNo: 1
  };

  constructor(private dataService: AppProviderService,
              private titleService: Title,
              private meta: Meta) { }

  ngOnInit() {
  }

  receiveData(filterData) {
    this.makeSeo(filterData);
    this.updateFilter(filterData);
    this.getData();
  }

  updateFilter(filter, pageNo?: number) {
    this.filter = {
      currentLocation: filter.currentLocation,
      searchText: filter.searchText,
      sortFees: filter.sortFees,
      gender: filter.gender,
      pageNo: 1
    };
  }

  getPageChange(pgno) {
    this.filter.pageNo = pgno;
    this.getData();
  }

  getData() {
    this.dataService.getDoctorList(this.filter).subscribe((res) => {
      if (res.status && res.data) {
        this.doctorData = res.data;
        this.pageConfig.totalItems = res.totalPage * 10;
      }
    });
  }


  makeSeo(filterData) {
    let location = 'List of Doctors';
    if ( filterData.currentLocation !== '') {
      location =  `${location} at ${filterData.currentLocation}`;
    }

    let specilization = 'Specilized';
    if ( filterData.searchText !== '') {
      specilization =  `${specilization} in ${filterData.searchText}`;
    }
    this.titleService.setTitle(`MyClnq Doctor's Profile :: ${location} | ${specilization}`);
    const keywords = `${location}, ${specilization}`;
    this.meta.addTag({name: 'description', content: `${keywords}`});
    this.meta.addTag({name: 'keywords', content: keywords});
    this.meta.addTag({name: 'author', content: 'SSIVIXLAB'});
    this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

}
