import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EnquiryComponent } from '../enquiry/enquiry.component';
import { AppProviderService } from '../dataService/data.service';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent implements OnInit, OnChanges {

  @Input() doctorList: any[];
  @Input() pageinationConfig: any;
  @Output() messageToEmit = new EventEmitter();
  doctorListData = [];
  isdataLoaded = false;

  constructor(private modalService: NgbModal, private appService: AppProviderService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.doctorList.currentValue ) {
      this.doctorListData = changes.doctorList.currentValue;
      // this.pageinationConfig
      this.isdataLoaded = true;
    }
    if (changes.doctorList.currentValue === undefined ) {
      this.isdataLoaded = false;
    }
  }

  getLink(ev) {
    const name = `${ev.name}`.replace(/[^\w\s]/gi, '').replace(/ /g, '_');
    const myurl = `/profile-view/${ev._id}/${name}`;
    return myurl;
  }

  checkAvailablity(ev) {
    const res = _.find(ev, {  day:  _.upperCase(moment().format('dddd')), isActive: true});
    if (res) {
      const startTime = moment(`${res.timings[0].startHour}:${res.timings[0].startMinute}`, 'HH:mm');
      const endTime = moment(`${res.timings[0].endHour}:${res.timings[0].endMinute}`, 'HH:mm');
      const current = moment(new Date(), 'HH:mm');
      if (current.isAfter(startTime) && current.isBefore(endTime)) {
        return 'Available';
      } else {
        return 'Not Available';
      }
    } else {
      return 'Not Available';
    }

  }

  contactClickCall(doc) {
    this.appService.updateClickContact(doc._id).subscribe((res) => {
      //
    });
  }

  profileClickCall(doc) {
    this.appService.updateClickProfile(doc._id).subscribe((res) => {
      //
    });
  }

  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return  `with: ${reason}`;
  //   }
  // }

  openModal(payload) {
    const modalRef = this.modalService.open(EnquiryComponent);
    modalRef.componentInstance.selectedDoctor = payload;
    modalRef.result.then((result) => {
      if (result) {
        alert(result);
      }
    }, (err) => {
      console.log('err', err);
    });
  }

  pageChange($ev) {
    this.messageToEmit.emit($ev);
  }

}
