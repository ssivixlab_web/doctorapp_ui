import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { SearchComponent } from './search/search.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';

export const ComponentsRoutes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'search',
        component: SearchComponent,
        data: {
          title: 'Search',
          urls: []
        }
      },
      {
        path: 'profile-view/:id/:name',
        component: ProfileViewComponent,
        data: {
          title: 'Profile View',
          urls: []
        }
      },
      { path: '', redirectTo: 'search', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(ComponentsRoutes,
    {
      enableTracing: false, // <-- debugging purposes only
      useHash: true

    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }


