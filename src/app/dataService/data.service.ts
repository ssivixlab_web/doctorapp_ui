import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppHttpApiService } from '../common/service/http-api.service';
import { API_URL, EXTERNAL_URL, NO_INTERCEPT_LOADER } from '../common/contants';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})
export class AppProviderService {
    currentLocation$ = new BehaviorSubject(null);
    constructor(private httpApi: AppHttpApiService) {
        // this.getGeoDetails();
    }

    public getGeoDetails() {
        this.httpApi.getExternalData(EXTERNAL_URL.GEO).subscribe((res) => {
            this.currentLocation$.next(res);
        }, (err) => {
            this.currentLocation$.next(err.error);
        });
    }

    getDoctorList(filterData): Observable<any> {
        return this.httpApi.postData(API_URL.LIST, {
            currentLocation: filterData.currentLocation,
            searchText: filterData.searchText,
            sortFees: filterData.sortFees,
            gender : filterData.gender,
            pageNo: filterData.pageNo,
            size: 10
        });
    }

    getDoctorProfile(id): Observable<any> {
        return this.httpApi.getData(`${API_URL.PROFILE}/${id}`);
    }

    sendEnquiry(payload): Observable<any> {
        return this.httpApi.postData(`${API_URL.ENQUIRY}`, payload);
    }

    updateClickContact(id): Observable<any> {
        let HttpUploadOptions = {
            headers: new HttpHeaders()
        };
        let headers: HttpHeaders = new HttpHeaders();
        HttpUploadOptions.headers = headers.append('NO_INTERCEPT_LOADER', 'aaaa');

        let city: string = 'Singapore';
        this.currentLocation$.subscribe((res) => {
            if (res && res.city && res.city !== '') {
                city = res.city;
            }
          });
        let payload = {
            doctorId: id,
            country: city
        };
        return this.httpApi.postData(`${API_URL.PROFILE_CALL}`, payload, HttpUploadOptions.headers);
    }

    updateClickProfile(id): Observable<any> {
        let HttpUploadOptions = {
            headers: new HttpHeaders()
        };
        let headers: HttpHeaders = new HttpHeaders();
        HttpUploadOptions.headers = headers.append('NO_INTERCEPT_LOADER', 'aaaa');

        let city: string = 'Singapore';
        this.currentLocation$.subscribe((res) => {
            if (res && res.city && res.city !== '') {
                city = res.city;
            }
          });
        let payload = {
            doctorId: id,
            country: city
        };
        return this.httpApi.postData(`${API_URL.PROFILE_VISIT}`, payload, HttpUploadOptions.headers);
    }

}
