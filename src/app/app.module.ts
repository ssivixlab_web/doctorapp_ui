import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SearchNavComponent } from './search-nav/search-nav.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { ProfileListComponent } from './profile-list/profile-list.component';
import { LayoutComponent } from './layout/layout.component';
import { SearchComponent } from './search/search.component';
import { AppMaterialModule } from './common/app-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppHttpApiService } from './common/service/http-api.service';
import { AppInterceptService } from './common/service/intercept.service';
import { AppProviderService } from './dataService/data.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService } from './common/loader/loader.service';
import { LoaderInterceptor } from './common/loader/loader.interceptor';
import { LoaderComponent } from './common/loader/loader.component';
import { AppPublishSubscribeService } from './common/pub-sub/publish-subscribe.service';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { DownloadComponent } from './download/download.component';
import { SafePipe } from './common/safe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    HeaderComponent,
    FooterComponent,
    SearchNavComponent,
    ProfileViewComponent,
    ProfileListComponent,
    LayoutComponent,
    SearchComponent,
    EnquiryComponent,
    DownloadComponent,
    SafePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    NgbModule
  ],
  entryComponents: [
    EnquiryComponent,
    DownloadComponent
  ],
  providers: [
    AppHttpApiService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptService,
      multi: true
    },
    AppProviderService,
    LoaderService,
    AppPublishSubscribeService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
