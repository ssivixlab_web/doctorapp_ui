import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoaderService } from './loader.service';
import { NO_INTERCEPT_LOADER } from '../contants';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
    constructor(public loaderService: LoaderService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.headers.get('NO_INTERCEPT_LOADER') !== null) {
            const newHeaders = req.headers.delete('NO_INTERCEPT_LOADER');
            const newRequest = req.clone({ headers: newHeaders });
            return next.handle(newRequest);
        } else {
            this.loaderService.show();
            return next.handle(req).pipe(
                finalize(() => this.loaderService.hide())
            );
        }
    }
}
