import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class AppInterceptService implements HttpInterceptor {
     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // const authToken = AppAuthenticationService.getSessionId();
        // if (authToken && authToken !== '') {
        //     request = request.clone({
        //         setHeaders: {
        //             authToken: `${authToken}`
        //         }
        //     });
        // }
        return next.handle(request).pipe(
            tap(evt => {}),
            // catchError((err: any) => {
            //     if (err instanceof HttpErrorResponse) {
            //         return throwError(new Error('oops!'));
            //     }
            // })
        );
    }
}
