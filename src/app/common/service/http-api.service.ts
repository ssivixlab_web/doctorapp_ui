import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API_END_POINT_URL } from '../contants';

@Injectable({
    providedIn: 'root'
})
export class AppHttpApiService {
    private endPoint = API_END_POINT_URL;
    private customReqOptions: HttpHeaders = null;

    constructor( private httpClient: HttpClient) {}

    private getHeaders( headers?: HttpHeaders): HttpHeaders {
        if (!headers) {
            headers = new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
        }
        return headers;
    }

    public getExternalData(externalUrl: string, headers?: HttpHeaders): Observable<any>;
    public getExternalData<T>(externalUrl: string, headers?: HttpHeaders): Observable<T>;
    public getExternalData<T>(externalUrl: string, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.get(`${externalUrl}`, { headers: this.customReqOptions});
    }

    public getData(url: string, headers?: HttpHeaders): Observable<any>;
    public getData<T>(url: string, headers?: HttpHeaders): Observable<T>;
    public getData<T>(url: string, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.get(`${this.endPoint}${url}`, { headers: this.customReqOptions});
    }

    public postData(url: string, payload: any, headers?: HttpHeaders): Observable<any>;
    public postData<T>(url: string, payload: any, headers?: HttpHeaders): Observable<T>;
    public postData<T>(url: string, payload: any, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.post(`${this.endPoint}${url}`, payload, { headers: this.customReqOptions} );
    }

    public putData(url: string, payload: any, headers?: HttpHeaders): Observable<any>;
    public putData<T>(url: string, payload: any, headers?: HttpHeaders): Observable<T>;
    public putData<T>(url: string, payload: any, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.put(`${this.endPoint}${url}`, payload , { headers: this.customReqOptions});
    }

    public deleteData(url: string, payload: any, headers?: HttpHeaders): Observable<any>;
    public deleteData<T>(url: string, payload: any, headers?: HttpHeaders): Observable<T>;
    public deleteData<T>(url: string, headers?: HttpHeaders): Observable<T> | Observable<any> {
        this.customReqOptions = this.getHeaders(headers);
        return this.httpClient.delete(`${this.endPoint}${url}`, { headers: this.customReqOptions});
    }
}
