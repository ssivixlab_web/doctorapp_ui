import { Injectable } from '@angular/core';
import { ReplaySubject, Subject, Subscription } from 'rxjs';

@Injectable()
export class AppPublishSubscribeService {
    private events = {};

    subscribeEvData( event: string,
                     callback?: (value: any) => void,
                     error?: (value: any) => void,
                     complete?: () => void,
                     cache: boolean = true): Subscription {
        if (this.events[event] === undefined) {
            cache ? this.events[event] = new ReplaySubject<any>() : this.events[event] = new Subject<any>();
        }

        if (typeof callback !== 'function') {
            return this.events[event].asObservable();
        } else {
            return this.events[event].asObservable().subscribe(callback, error, complete);
        }
    }

    publishEvData(event: string, eventDataObj?: any): void {
        if (!this.events[event]) {
            return;
        }
        this.events[event].next(eventDataObj);
    }
}
