import { environment } from 'src/environments/environment';

export const API_END_POINT_URL = environment.endPointUrl;
// All get/post/put/delete End point
export const API_URL = {
    LIST: 'list',
    PROFILE: 'profile',
    ENQUIRY: 'enquiery',
    PROFILE_CALL: 'profile/call',
    PROFILE_VISIT: 'profile/visit',
};


export const EXTERNAL_URL = {
    // GEO: 'https://ipinfo.io/geo'
    GEO: 'https://ip-api.io/json'
};

export const NO_INTERCEPT_LOADER = 'NO_INTERCEPT_LOADER';